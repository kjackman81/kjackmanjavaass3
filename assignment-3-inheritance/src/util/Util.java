package util;

import ie.wit.ictskills.shapes.Measurable;
import ie.wit.ictskills.shapes.Shapes;

import java.util.ArrayList;

public class Util {
	/**
	 * Measureable interface contains the method double perimeter(). The method
	 * maximum here evalutates the single value representing the largest
	 * perimeter discovered in the list of Measurable objects.
	 *
	 * @param object
	 *            The list of objects whose classes implement the interface
	 *            Measurable
	 * @return double Returns the largest perimeter discovered among entire list
	 *         objects.
	 */
	static public double maximum(ArrayList<Measurable> measurables) {
		double max = 0;
		// TODO Task 6: Implement method Util.maximum

		for (Measurable measure : measurables) {
			double val = measure.perimeter();
			max = val > max ? val : max;
		}
		return max;

	}

}
