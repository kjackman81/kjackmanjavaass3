package ie.wit.ictskills.shapes;

// TODO Task 1: Refactor: derive from Shapes super class
/**
 * rectangle class inherits from shapes implements the measurable interface
 */
public class Rectangle extends Shapes implements Measurable {
	// object variables
	private int xSideLength;
	private int ySideLength;

	/**
	 * rectangle default constructor creates a new rectangle with hard coded
	 * parameters uses the superclass constructor inherited from shapes
	 * int,int,String,boolean calls the setState method for both xSidlength and
	 * ySidelength
	 * 
	 */

	public Rectangle() {
		super(50, 100, "red", true);
		setState(100, 50);
	}

	/**
	 * constructor with specified parameters uses the superclass constructor
	 * inherited from shapes
	 * 
	 * @param int
	 *            xSideLength the length of the x side
	 * @param int
	 *            ySideLength the length of the y side
	 * @param int
	 *            xPosition the x position on the canvas
	 * @param int
	 *            yPosition the y position on the canvas
	 * @param String
	 *            color the color of the object
	 * @param boolean
	 *            set the visibility of the object
	 */
	public Rectangle(int xSideLength, int ySideLength, int xPosition, int yPosition, String color) {
		super(xPosition, yPosition, color, true);

		setState(xSideLength, ySideLength);
	}

	/**
	 * setState method used to initialize formal parameters
	 * 
	 * @param int
	 *            xsideLength the length of the x side
	 * @param int
	 *            ysideLength the length of the y side
	 */
	public void setState(int xSideLength, int ySideLength) {

		this.xSideLength = xSideLength;
		this.ySideLength = ySideLength;

	}

	/**
	 * draws the shape on the canvas abstract method from shapes implemented
	 */
	@Override
	public void draw() {
		if (isVisible) {
			Canvas canvas = Canvas.getCanvas();
			canvas.draw(this, color, new java.awt.Rectangle(xPosition, yPosition, xSideLength, ySideLength));
			canvas.wait(10);
		}
	}

	/**
	 * abstract method from shapes allows you to change the circle size.
	 * 
	 * @param int
	 *            scale the size you want to increase it by
	 */
	@Override
	public void changeSize(int scale) {
		super.erase();
		this.xSideLength *= scale;
		this.ySideLength *= scale;
		draw();
	}

	/**
	 * method overrode from measurable interface used to find perimeter of
	 * rectangle
	 * 
	 * @return double the perimeter of the rectangle
	 */
	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return ((xSideLength + ySideLength) * 2);
	}

	/**
	 * main method used to instantiate default rectangle draws it and makes it
	 * visible by using mentioned methods
	 * 
	 * @param String
	 *            args
	 */
	public static void main(String args[]) {
		Rectangle r1 = new Rectangle();
		r1.draw();
		r1.makeVisible();

	}
}