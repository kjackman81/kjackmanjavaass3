package ie.wit.ictskills.shapes;

import java.util.ArrayList;

public class TestPentagon {

	public static void main(String[] args) {
		// TODO Task 3: Display 4 cascaded Pentagons differently colored shapes.

		ArrayList<Shapes> shapes = new ArrayList<>();

		shapes.add(new Pentagon(40, 90, 60, "red"));
		shapes.add(new Pentagon(50, 120, 90, "blue"));
		shapes.add(new Pentagon(60, 160, 120, "green"));
		shapes.add(new Pentagon(70, 200, 150, "black"));
		
		// calls the makeVisible method on each pentagon object in the shapes array
	
		for (Shapes shape : shapes) {
			shape.makeVisible();
		}

	}

}
