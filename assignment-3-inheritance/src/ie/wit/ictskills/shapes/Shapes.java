package ie.wit.ictskills.shapes;

/**
 * An abstract class describing geometric shapes
 * 
 * @author jfitzgerald
 * @version 2016-04-12
 */

// abstract class can not be instantiated
public abstract class Shapes {
	// package private variables
	int xPosition;
	int yPosition;
	String color;
	boolean isVisible;

	/**
	 * constructor with parameters initializes variables
	 * 
	 * @param int
	 *            xPosition the x position of the circle
	 * @param int
	 *            yPosition the y position of the circle
	 * @param String
	 *            color the color of the circle
	 * @param boolean
	 *            isVisible used to set the visibility of object
	 */

	public Shapes(int xPosition, int yPosition, String color, boolean isVisible) {
		this.xPosition = xPosition;
		this.yPosition = yPosition;
		this.color = color;
		this.isVisible = isVisible;
	}

	/**
	 * abstract method to be implemented
	 */
	abstract void draw();

	/**
	 * abstract method to be implemented
	 */

	abstract void changeSize(int scale);

	/**
	 * used to set the color of the object
	 * 
	 * @param color
	 *            the color to set  the object
	 */

	public void changeColor(String color) {
		this.color = color;
		draw();
	}

	/**
	 * method used to erase canvas
	 * 
	 */
	protected void erase() {
		if (isVisible) {
			Canvas canvas = Canvas.getCanvas();
			canvas.erase(this);
		}
	}

	/**
	 * method used to make the object visible sets the isVisible variable to true
	 * then calls its draw method
	 */
	public void makeVisible() {
		isVisible = true;
		draw();

	}

}
