package ie.wit.ictskills.shapes;

import java.awt.Polygon;

/**
 * triangle class inherits from shapes implements the measurable interface
 */
public class Triangle extends Shapes implements Measurable {
	// object variables for implementation
	private int height;
	private int width;

	/**
	 * triangle default constructor creates a new triangle with hard coded
	 * parameters uses the superclass constructor inherited from shapes
	 * int,int,String,boolean calls the setState method for both height and
	 * width
	 */

	public Triangle() {
		// super(xPosition, yPosition, color, isVisible)
		super(150, 65, "black", true);
		setState(50, 75);
	}

	/**
	 * constructor with specified formal parameters uses the superclass
	 * constructor inherited from shapes
	 * 
	 * @param int
	 *            height the height of the triangle
	 * @param int
	 *            width the width of the triangle
	 * @param int
	 *            xPosition the x position on the canvas
	 * @param int
	 *            yPosition the y position on the canvas
	 * @param String
	 *            color the color of the object
	 * @param boolean
	 *            set the visibility of the object
	 */
	public Triangle(int height, int width, int xPosition, int yPosition, String color) {
		// super(xPosition, yPosition, color, isVisible)
		super(xPosition, yPosition, color, true);
		setState(height, width);

	}

	/**
	 * used to initialize the height and width of the triangle
	 * 
	 * @param height
	 *            to set the height of the triangle
	 * @param width
	 *            to set the width of the triangle
	 */
	public void setState(int height, int width) {
		this.height = height;
		this.width = width;
	}

	/**
	 * draws the shape on the canvas abstract method from shapes implemented
	 */
	void draw() {
		if (isVisible) {
			Canvas canvas = Canvas.getCanvas();
			int[] xpoints = { xPosition, xPosition + (width / 2), xPosition - (width / 2) };
			int[] ypoints = { yPosition, yPosition + height, yPosition + height };
			canvas.draw(this, color, new Polygon(xpoints, ypoints, 3));
			canvas.wait(10);
		}
	}

	/**
	 * changes the size of the triangle with abstract method from shapes implemented
	 * calls the super method to erase, scales the triangle and re-draws
	 * 
	 * @param int
	 *            scale the size you want to increase it by
	 */
	@Override
	public void changeSize(int scale) {
		super.erase();
		height *= scale;
		width *= scale;
		draw();
	}

	/**
	 * method overrode from measurable interface used to find perimeter of
	 * triangle
	 * 
	 * @return double the perimeter of the triangle
	 */
	@Override
	public double perimeter() {
		return 2 * Math.hypot(height, width / 2) + width;
	}

}
