package ie.wit.ictskills.shapes;

public interface Measurable {
	//method signature to be overrode
	double perimeter();
}