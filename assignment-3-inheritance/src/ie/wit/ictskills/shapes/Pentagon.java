package ie.wit.ictskills.shapes;

// TODO Task 2: Complete Pentagon class
import java.awt.Polygon;

/**
 * @file Pentagon.java
 * @brief This class describes a pentagon and has behaviour to display, resize
 *        and move objects
 * 
 * @author jfitzgerald 2014-05-23
 * 
 */

/**
 * pentagon class inherits from shapes implements the measurable interface
 */
public class Pentagon extends Shapes implements Measurable // extends Shapes
{
	private int radius;// radius of circumscribing circle object variable

	/**
	 * default constructor with hard coded parameters calls the super
	 * constructor in shapes creates an object of type pentagon
	 * int,int,String,boolean
	 */

	public Pentagon() {
		super(50, 50, "yellow", true);
		this.radius = 50;
	}

	/**
	 * constructor with parameters creates an object of type pentagon calls the
	 * super constructor in shapes
	 * 
	 * @param int
	 *            radius the radius of the pentagon
	 * @param int
	 *            xPosition the x position of the pentagon
	 * @param int
	 *            yPosition the y position of the pentagon
	 * @param String
	 *            color the color of the pentagon
	 * 
	 */
	public Pentagon(int radius, int xPosition, int yPosition, String color) {
		super(xPosition, yPosition, color, true);
		this.radius = radius;
	}

	/**
	 * changes the size of the pentagon abstract method from shapes implemented
	 * 
	 * @param int
	 *            scale the size you want to increase it by
	 */
	@Override
	public void changeSize(int scale) {
		this.radius *= scale;
		super.erase();
		this.radius *= scale;
		draw();
	}

	/**
	 * draws the shape on the canvas abstract method from shapes implemented
	 */
	@Override
	public void draw() {
		if (isVisible) {
			// Ref: http://mathworld.wolfram.com/Pentagon.html
			double dc1 = 0.25 * (Math.sqrt(5) - 1);
			double dc2 = 0.25 * (Math.sqrt(5) + 1);
			double ds1 = 0.25 * (Math.sqrt(10 + 2 * Math.sqrt(5)));
			double ds2 = 0.25 * (Math.sqrt(10 - 2 * Math.sqrt(5)));// length of
																	// pentagon
																	// side is
																	// 2*ds2
			int c1 = -(int) (radius * dc1);// radius of circle that
											// circumscribes pentagon
			int c2 = -(int) (radius * dc2);
			int s1 = (int) (radius * ds1);
			int s2 = (int) (radius * ds2);

			Canvas canvas = Canvas.getCanvas();
			int[] xpoints = { xPosition, xPosition + s1, xPosition + s2, xPosition - s2, xPosition - s1 };
			int[] ypoints = { yPosition - radius, yPosition + c1, yPosition - c2, yPosition - c2, yPosition + c1 };
			canvas.draw(this, color, new Polygon(xpoints, ypoints, 5));
			canvas.wait(10);
		}
	}

	/**
	 * method overrode from measurable interface used to find perimeter of
	 * pentagon
	 * 
	 * @return double the perimeter of the pentagon
	 */
	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 10 * radius * Math.sin(Math.PI / 5);
	}
}
