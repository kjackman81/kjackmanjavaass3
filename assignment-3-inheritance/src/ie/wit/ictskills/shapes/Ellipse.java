package ie.wit.ictskills.shapes;

// TODO Task 4: Complete Ellipse, inherit Shapes, implement Measurable, subclass Circle.
import ie.wit.ictskills.util.ellipse.EllipseMeasure;

import java.awt.geom.*;

/**
 * @file Elipse.java
 * @brief This class describes a ellipse and has behaviors to display, resize
 *        and move objects
 * 
 * @author jfitzgerald 2014-05-23
 * 
 */

/**
 * ellipse class inherits from shapes implements the measurable interface
 */
public class Ellipse extends Shapes implements Measurable {
	// object variables
	protected int xdiameter;
	private int ydiameter;

	/**
	 * ellipse default constructor creates a new ellipse with hard coded
	 * parameters uses the superclass constructor inherited from ellipse
	 * int,int,int,int,Sting,boolean
	 */
	public Ellipse() {
		super(180, 100, "yellow", true);
		setState(50, 100);

	}

	/**
	 * constructor with specified formla parameters uses the superclass
	 * constructor inherited from shapes
	 * 
	 * @param int
	 *            xdiameter the x diameter of the ellipse
	 * @param int
	 *            ydiameter the y diameter of the ellipse
	 * @param int
	 *            xPosition the x position of the ellipse
	 * @param int
	 *            yPosition the y position of the ellipse
	 * @param String
	 *            color the color of the ellipse
	 */
	public Ellipse(int xdiameter, int ydiameter, int xPosition, int yPosition, String color) {
		super(xPosition, yPosition, color, true);
		setState(xdiameter, ydiameter);

	}

	/**
	 * setState method used to initialize formal parameters
	 * 
	 * @param int
	 *            xdiameter the x diameter of the ellipse
	 * @param int
	 *            ydiameter the y diameter of the ellipse
	 */
	private void setState(int xdiameter, int ydiameter) {

		this.xdiameter = xdiameter;
		this.ydiameter = ydiameter;

	}

	/**
	 * abstract method from shapes allows you to change the circle size. calls
	 * the erase method from superclass aswell as its own draw method
	 * 
	 * @param int
	 *            scale the size you want to increase it by
	 */
	@Override
	public void changeSize(int scale) {

		super.erase();
		this.xdiameter *= scale;
		this.ydiameter *= scale;
		draw();

	}

	/**
	 * draws the shape on the canvas abstract method from shapes implemented
	 */
	@Override
	void draw() {
		if (isVisible) {
			Canvas canvas = Canvas.getCanvas();
			canvas.draw(this, color, new Ellipse2D.Double(xPosition, yPosition, xdiameter, ydiameter));
			canvas.wait(10);
		}
	}

	/**
	 * method overrode from measurable interface used to find perimeter of the
	 * ellipse
	 * 
	 * @return double perimeter of the ellipse
	 */
	@Override
	public double perimeter() {
		return EllipseMeasure.perimeter(xdiameter, ydiameter);

	}

}
