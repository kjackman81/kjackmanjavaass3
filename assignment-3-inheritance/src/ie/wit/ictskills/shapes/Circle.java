package ie.wit.ictskills.shapes;

import java.awt.geom.Ellipse2D;

import ie.wit.ictskills.util.ellipse.EllipseMeasure;

/**
 * A circle that can be manipulated and that draws itself on a canvas.
 * 
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 */

/**
 * circle class inherits from ellipse implements the measurable interface
 */
public class Circle extends Ellipse implements Measurable {

	/**
	 * circle default constructor creates a new circle with hard coded
	 * parameters uses the superclass constructor inherited from ellipse
	 * int,int,int,int,String
	 */
	public Circle() {
		super(40, 40, 50, 50, "red");

	}

	/**
	 * constructor with specified formal parameters uses the superclass constructor
	 * inherited from ellipse
	 * 
	 * @param int
	 *            diameter the diameter of the circle
	 * @param int
	 *            xPosition the x position of the circle
	 * @param int
	 *            yPosition the y position of the circle
	 * @param String
	 *            color the color of the circle
	 */
	public Circle(int diameter, int xPosition, int yPosition, String color) {
		super(diameter, diameter, xPosition, yPosition, color);

	}

	/**
	 * main method used to instantiate default circle draws it and makes it
	 * visible by using mentioned methods
	 * 
	 * @param String
	 *            args
	 */
	public static void main(String[] args) {
		Circle circle = new Circle();
		// uses the changeSize method from the superclass
	    // circle.changeSize(5);
		circle.draw();// draw method called from superclass
		circle.makeVisible();//makeVisible method called from superclass
		// uses the perimeter() method from the superclass
		// System.out.print(circle.perimeter());
		
	}
}
