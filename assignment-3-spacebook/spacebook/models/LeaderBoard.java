package models;
/**
* @file    LeaderBoard.java
* @brief   Class publishes users graded most socially and talkatively active
* @version 2015-4-22
* @author  
*/

import java.util.ArrayList;
import java.util.Collections;

import utils.UserLeastTalkativeComparator;
import utils.UserSocialComparator;
import utils.UserTalkativeComparator;
import views.LeaderBoardView;

public class LeaderBoard {
	/**
	 * used to sort an ArrayList by a particular parameter there amount of
	 * friends(friendships)
	 * 
	 * @param ArrayList<User>
	 *            users the arrayList to be sorted by the names of the users
	 */
	public static void index(ArrayList<User> users) {

		Collections.sort(users, new UserSocialComparator());
		LeaderBoardView.index(users);
	}
	/**
	 * used to sort an ArrayList by a particular parameter, there amount of
	 * messages(outbox)...most
	 * 
	 * @param ArrayList<User>
	 *            users the arrayList to be sorted by the names of the users
	 */
	public static void talkative(ArrayList<User> users) {
		
		Collections.sort(users, new UserTalkativeComparator());
		LeaderBoardView.talkative(users);

	}
	/**
	 * used to sort an ArrayList by a particular parameter, there amount of
	 * messages(outbox)....least
	 * 
	 * @param ArrayList<User>
	 *            users the arrayList to be sorted by the names of the users
	 */
	public static void leastTalkative(ArrayList<User> users) {
		
		Collections.sort(users, new UserLeastTalkativeComparator());
		LeaderBoardView.leastTalkative(users);
	}
}
