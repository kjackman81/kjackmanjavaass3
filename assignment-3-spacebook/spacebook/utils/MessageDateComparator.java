package utils;

import java.util.Comparator;

import models.Message;

public class MessageDateComparator implements Comparator<Message> {
	/**
	 * compares the dates of two messages from oldest to newest 
	 * uses the date field in message
	 * 
	 * @param Message
	 *            a the messages date being compared
	 * @param Message
	 *            b the messages date being compared
	 * 
	 * @return int  0 if messaage date (a) is equal to messaage date(b) 
	 *              less than zero if messaage date (a) less than  messaage date (b)
	 *              greater than zero if  messaage date (a) greater than  messaage date (b)
	 */
	@Override
	public int compare(Message a, Message b) {

		return a.postedAt.compareTo(b.postedAt);

	}
}
