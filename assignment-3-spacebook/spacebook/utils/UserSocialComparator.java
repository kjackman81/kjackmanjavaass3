package utils;

import java.util.Comparator;

import models.User;

public class UserSocialComparator implements Comparator<User>
{
	/**
	 * compares two user friendships arraylist size which is bigger 
	 * 
	 * @param User 
	 *            a the friendships arraylist size  being compared
	 * @param User
	 *            b the friendships arraylist size  being compared
	 *            
	 * @return int 0 if User (a) friendships size is equal to User (b) friendships size
	 *         less than zero if User (a) friendships size less than User (b)friendships
	 *         size greater than zero if User (a) friendships size greater than User
	 *         (b)friendships size
	 **/
	@Override
  public int compare(User a, User b)
  {
    return Integer.compare (b.friendships.size(), a.friendships.size());
  }
}
