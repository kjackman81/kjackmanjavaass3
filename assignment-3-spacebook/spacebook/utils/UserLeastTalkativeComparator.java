package utils;

import java.util.Comparator;

import models.User;

public class UserLeastTalkativeComparator implements Comparator<User> {

	/**
	 * compares two user outbox sizes and arranges in acceding order
	 * from lowest to highest
	 * 
	 * @param User
	 *            a the messages outbox size being compared
	 * @param User
	 *            b the messages outbox size being compared
	 * 
	 * @return int 0 if User (a) oubox size is equal to User (b) outbox size
	 *         less than zero if User (a) outbox size less than User (b)outbox
	 *         size greater than zero if User (a) outbox size greater than User
	 *         (b)outbox size
	 */
	@Override
	public int compare(User a, User b) {

		return Integer.compare(a.outbox.size(), b.outbox.size());

	}
}
